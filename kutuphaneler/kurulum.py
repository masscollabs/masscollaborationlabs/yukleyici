import gi, os, pwd
from gi.repository import Vte, GLib, Gdk, Gio, Gtk
from kutuphaneler import diller

class StKurulum(Gtk.Grid):
	def __init__(self,ebeveyn):
		Gtk.Window.__init__(self)
		self.ebeveyn = ebeveyn
		self.baslik = "Milis Linux Kuruluyor"
		self.ad = "Kurulum"
		self.slayt = 0
		self.slaytlar = Gtk.Image.new_from_file("{}/slayt_0.png".format(self.ebeveyn.resim_yol))
		self.attach(self.slaytlar,0,0,1,1)
		self.surec = Gtk.ProgressBar()
		self.attach(self.surec,0,1,1,1)
		self.terminal = Terminal(self)
		scroll = Gtk.ScrolledWindow()
		scroll.set_min_content_width(630)
		scroll.set_min_content_height(50)
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.terminal)
		self.attach(scroll,0,2,1,1)

	def dil_ata(self,dil):
		self.baslik = diller.diller[dil]["t48"]

	def surec_ata(self,deger):
		if "%" in deger:
			try:
				d = deger.split()
				d = d[1].split(".")
				yuzde = int(d[0])
				d = yuzde / 100
				self.surec.set_fraction(d)
				slayt = yuzde // 30
				if self.slayt != slayt:
					self.slaytlar.set_from_file("{}/slayt_{}.png".format(self.ebeveyn.resim_yol,str(slayt)))
					self.slayt = 0
			except:
				pass

class Terminal(Vte.Terminal):
	def __init__(self,ebeveyn):
		Vte.Terminal.__init__(self)
		#Ebeveyndeki değişkenlere ulaşmak için
		self.ebeveyn = ebeveyn
		#Komutları ebeveyn buraya dolduracak bitene kadar dönecek
		self.komutlar = []
		#Spawn sync ile terminali oluşturuyoruz
		self.spawn_sync(Vte.PtyFlags.DEFAULT,
						os.path.expanduser('~'),
						["/bin/bash"],
						[],
						GLib.SpawnFlags.DO_NOT_REAP_CHILD,
						None,
						None)
		#Renkler için bir paket oluşturacağız
		palet = [
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			]
		#Renklerimizi bir listeye veriyoruz
		renkler = [
			"#f8f8f2",
			"#272822",
			"#000000",
			"#f92672",
			"#a6e22e",
			"#e6db74",
			"#66d9ef",
			"#ae81ff",
			"#a1efe4",
			"#f8f8f2",
			"#000000",
			"#c61e5b",
			"#80af24",
			"#b3aa5a",
			"#50abbc",
			"#8b67cc",
			"#7fbcb3",
			"#cbcdc8",
			]
		#Paletlere renkleri ekleyelim
		for sayi in range(0,16):
			oldu_mu = palet[sayi].parse(renkler[sayi])
			if not oldu_mu:
				print("Renk okunamadı :: {}".format(renkler[sayi]))

		#Renkleri terminale ekleyelim
		self.set_colors(palet[0],palet[1],palet[2:])
		#Scroll hareket ettiğinde sinyal yayılacak bizde bittimi anlayacağız
		self.connect("child-exited", self.komut_bitti)
		self.connect("contents-changed", self.terminal_guncellendi)

	def terminal_guncellendi(self,term):
		text = self.get_text_include_trailing_spaces(None,None)[0]
		bol = text.split("\n")
		son = ""
		bol = bol[::-1]
		for satir in bol:
			if satir != '':
				son = satir
				break
		self.ebeveyn.surec_ata(son)

	def komut_bitti(self,term,pid):
		"""Komutun bitip bitmediğini anlamaya çalışacağız"""
		#Bittiyse dosya yazımı tamamlanmıştı dosyayı okuyalım
		okunan = self.get_text_include_trailing_spaces(None,None)[0]
		#Çalışan komut komutların 0. arkadaşı
		okunan = okunan.split("\n")
		if "OK" in okunan:
			print("KURULUM BAŞARILI")
			self.ebeveyn.ebeveyn.ileri_basildi(None)
		elif len(okunan) < 3:
			pass
		else:
			baslik = diller.diller[self.ebeveyn.ebeveyn.milis_ayarlari["dil"]]["t77"]
			soru = Gtk.MessageDialog(self.ebeveyn.ebeveyn,0,Gtk.MessageType.WARNING, Gtk.ButtonsType.OK,baslik)
			soru.set_title(diller.diller[self.ebeveyn.ebeveyn.milis_ayarlari["dil"]]["t77"])
			soru.format_secondary_text(diller.diller[self.ebeveyn.ebeveyn.milis_ayarlari["dil"]]["t78"])
			cevap = soru.run()
			if cevap == Gtk.ResponseType.OK:
				soru.destroy()
			self.ebeveyn.ebeveyn.stack_secili = 1
			self.ebeveyn.ebeveyn.geri_basildi(None)
			print("KURULUM BAŞARISIZ")

	def komut_calistir(self):
		"""Komut çalıştırılmak için bu fonksiyonu yazacağız"""
		user_info = pwd.getpwuid(os.geteuid())
		a = self.spawn_sync(
						Vte.PtyFlags.DEFAULT,
						user_info.pw_dir,
						[user_info.pw_shell,"/tmp/milis_kurulum_baslat.sh"],
						[],
						GLib.SpawnFlags.DEFAULT,
						None,
						None)
		self.watch_child(a[1])

